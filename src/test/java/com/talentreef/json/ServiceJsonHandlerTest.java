package com.talentreef.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.talentreef.kong.ImmutableAuthnValue;
import com.talentreef.kong.ImmutableRouteValue;
import com.talentreef.kong.ImmutableServiceValue;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ServiceJsonHandlerTest {

    private static ServiceJsonHandler serviceJsonHandler;
    private static String json;

    private static ImmutableServiceValue serviceValue;

    private static ObjectMapper objectMapper;

    @BeforeAll
    public static void beforeAll() {
        serviceJsonHandler = new ServiceJsonHandler();
        json = "{" +
                "\"id\":\"123\"," +
                "\"name\":\"test-service\"," +
                "\"uri\":\"http://huhuhu.com\"," +
                "\"routes\":[" +
                "{\n" +
                "    \"id\" : \"123\",\n" +
                "    \"methods\" : [ \"PUT\" ],\n" +
                "    \"hosts\" : [ \"job-sponsorship-service\" ],\n" +
                "    \"paths\" : [ \"/v1/clients/[^/]*/providers/[^/]*/jobs/[^/]*/sponsorships/[^/]*/status/[^/]*\" ],\n" +
                "    \"authn\" : {\n" +
                "      \"authenticated\" : false,\n" +
                "      \"authorizedGroups\" : [ ]\n" +
                "    }\n" +
                "  }" +
                "]" +
                "}";
        serviceValue = new ImmutableServiceValue.Builder()
            .id("123")
            .name("test-service")
            .uri("http://huhuhu.com")
            .routes(
                    List.of(
                            new ImmutableRouteValue.Builder()
                                    .id("123")
                                    .hosts(List.of("job-sponsorship-service"))
                                    .paths(List.of("/v1/clients/[^/]*/providers/[^/]*/jobs/[^/]*/sponsorships/[^/]*/status/[^/]*"))
                                    .methods(List.of("PUT"))
                                    .authn(
                                            new ImmutableAuthnValue.Builder()
                                                    .authenticated(false)
                                                    .authorizedGroups(List.of())
                                                    .build())
                                    .build()))
            .build();

        objectMapper = new ObjectMapper();
    }

    @Test
    public void serviceFromJson() throws IOException  {
        FileInputStream fileInputStream = tempFile(json);
        assertThat(serviceJsonHandler.serviceFromJson(fileInputStream))
                .isEqualTo(serviceValue);
    }

    @Test
    public void serviceToJson() throws IOException {
        ServiceJsonHandler serviceJsonHandler = new ServiceJsonHandler(objectMapper.writer());
        String jsonCompressed = objectMapper.readTree(json).toString();

        assertThat(serviceJsonHandler.serviceToJson(serviceValue))
                .isEqualTo(jsonCompressed);
    }

    private FileInputStream tempFile(String json) throws IOException {
        File temp = File.createTempFile("service", ".json");
        BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
        bw.write(json);
        bw.close();
        return new FileInputStream(temp);
    }
}
