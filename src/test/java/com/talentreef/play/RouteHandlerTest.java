package com.talentreef.play;

import com.talentreef.kong.ImmutableAuthnValue;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.assertj.core.api.Assertions.entry;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

public class RouteHandlerTest {

    private static RouteHandler routeHandler;

    private String configPath = "/v1/clients/[^/]*/providers/[^/]*/configuration";
    private String contactPath = "/v1/clients/[^/]*/providers/[^/]*/contact";

    @BeforeAll
    public static void beforeAll() {
        routeHandler = new RouteHandler();
    }

    @Test
    public void fromStream() throws IOException { // TODO: test exception
        String routes = "PUT             /v1/clients/:clientId/providers/:provider/configuration                                                     com.talentreef.job.sponsorship.controllers.JobSponsorshipSetupController.saveJobConfiguration(clientId: java.util.UUID, provider: String)\n" +
                "GET             /v1/clients/:clientId/providers/:provider/configuration                                                     com.talentreef.job.sponsorship.controllers.JobSponsorshipSetupController.getJobConfiguration(clientId: java.util.UUID, provider: String)\n" +
                "\n" +
                "PUT             /v1/clients/:clientId/providers/:provider/contact                                                           com.talentreef.job.sponsorship.controllers.JobSponsorshipSetupController.saveSponsorshipContact(clientId: java.util.UUID, provider: String)\n" +
                "\n";

        InputStream routesStream =
                IOUtils.toInputStream(routes, StandardCharsets.UTF_8);
        Map<String, List<String>> routeValues = routeHandler.fromStream(List.of(routesStream));

        assertThat(routeValues).hasSize(2);
        assertThat(routeValues)
                .contains(
                        entry(
                                configPath,
                                List.of("PUT", "GET")),
                        entry(
                                contactPath,
                                List.of("PUT")));


    }

    @Test
    public void toKongRoutes() {
        List<com.talentreef.kong.RouteValue> routes =
                routeHandler.toKongRoutes(
                        "JOB_SERVICE_V1", routeValues());

        assertThat(routes).hasSize(2);
        assertThat(routes)
                .extracting("hosts", "methods", "paths")
                .contains(
                        tuple(
                                List.of("JOB_SERVICE_V1"),
                                List.of("PUT", "GET"), // Order is significant here, I'm not a fan
                                List.of(configPath)));

    }

    private Map<String, List<String>> routeValues() {
        Map<String, List<String>> map = new HashMap<>();
        map.put(configPath, List.of("PUT", "GET"));
        map.put(contactPath, List.of("PUT"));

        return map;
    }
}
