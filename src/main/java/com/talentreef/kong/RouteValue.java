package com.talentreef.kong;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;
import java.util.UUID;

@Value.Immutable
@Value.Style(builder = "new")
@JsonDeserialize(builder = ImmutableRouteValue.Builder.class)
public abstract class RouteValue {
    @Value.Default
    public String id() {
        return UUID.randomUUID().toString();
    }
    public abstract List<String> methods();
    public abstract List<String> hosts();
    public abstract List<String> paths();
    public abstract AuthnValue authn();
}
