package com.talentreef.kong;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.List;

// There isn't a really very good way to tell jackson to translate snake case to camel case without writing a bunch of awful code
@Value.Immutable
@Value.Style(builder = "new")
@JsonDeserialize(builder = ImmutableKongResponse.Builder.class)
@JsonSerialize(as = ImmutableKongResponse.class)
public abstract class KongResponse {
    @JsonProperty("hosts")
    public abstract List<String> hosts();
    @JsonProperty("created_at")
    public abstract String createdAt();
    @JsonProperty("strip_uri")
    public abstract boolean stripUri();
    @JsonProperty("id")
    public abstract String id();
    @JsonProperty("name")
    public abstract String name();
    @JsonProperty("http_if_terminated")
    public abstract boolean httpIfTerminated();
    @JsonProperty("https_only")
    public abstract boolean httpsOnly();
    @JsonProperty("retries")
    public abstract int retries();
    @JsonProperty("uris")
    public abstract List<String> uris();
    @JsonProperty("upstream_url")
    public abstract String upstreamUrl();
    @JsonProperty("upstream_send_timeout")
    public abstract int upstreamSendTimeout();
    @JsonProperty("upstream_read_timeout")
    public abstract int upstreamReadTimeout();
    @JsonProperty("upstream_connect_timeout")
    public abstract int upstreamConnectTimeout();
    @JsonProperty("preserve_host")
    public abstract boolean preserveHost();
}
