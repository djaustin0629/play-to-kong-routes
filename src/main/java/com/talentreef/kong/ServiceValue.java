package com.talentreef.kong;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;
import java.util.UUID;

@Value.Immutable
@Value.Style(builder = "new")
@JsonDeserialize(builder = ImmutableServiceValue.Builder.class)
public abstract class ServiceValue {
    @Value.Default
    public String id() {
        return UUID.randomUUID().toString();
    }
    public abstract String name();
    public abstract String uri();
    public abstract List<RouteValue> routes();
}
