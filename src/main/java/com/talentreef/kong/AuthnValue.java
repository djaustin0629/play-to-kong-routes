package com.talentreef.kong;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

import java.util.List;

@Value.Immutable
@Value.Style(builder = "new")
@JsonDeserialize(builder = ImmutableAuthnValue.Builder.class)
public abstract class AuthnValue {
    @Value.Default
    public boolean authenticated() {
        return true;
    }
    public abstract List<String> authorizedGroups();
}
