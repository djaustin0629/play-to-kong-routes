package com.talentreef.kong;

import com.talentreef.json.ServiceJsonHandler;
import com.talentreef.play.RouteHandler;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ServiceDefinitionBuilder {

    private final ServiceJsonHandler serviceJsonHandler;
    private final RouteHandler routeHandler;
    private final InputStream kongTemplateJson;

    // TODO: add these to a factory since they're pretty much always passed around together
    public ServiceDefinitionBuilder(ServiceJsonHandler serviceJsonHandler,
                                    RouteHandler routeHandler) {

        this.serviceJsonHandler = serviceJsonHandler;
        this.routeHandler = routeHandler;
        // We shouldn't really need to change this as a source
        this.kongTemplateJson = getClass().getResourceAsStream(
                "/kong-service-template.json");
    }

    public ServiceValue fromStream(String serviceName,
                                   List<InputStream> playRoutes) throws IOException {

        return ImmutableServiceValue
                .copyOf(serviceJsonHandler.serviceFromJson(kongTemplateJson))
                .withRoutes(
                        routeHandler.toKongRoutes(
                                serviceName,
                                routeHandler.fromStream(playRoutes)));
    }

    public void write(File serviceFile,
                      ServiceValue serviceValueWithRoute) throws IOException {

        FileUtils.write(
                serviceFile,
                serviceJsonHandler.serviceToJson(serviceValueWithRoute),
                "UTF-8",
                false);
    }
}
