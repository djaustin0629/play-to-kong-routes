package com.talentreef.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.talentreef.kong.ServiceValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class ServiceJsonHandler {

    private final ObjectMapper mapper;
    private final ObjectWriter writer;

    public ServiceJsonHandler() {
        this.mapper = new ObjectMapper();
        this.writer = mapper.writerWithDefaultPrettyPrinter();
    }

    public ServiceJsonHandler(ObjectWriter objectWriter) {
        this.mapper = new ObjectMapper();
        this.writer = objectWriter;
    }

    public ServiceValue serviceFromJson(InputStream inputStream) throws IOException {
       return mapper.readValue(readJson(inputStream), ServiceValue.class);
    }

    public String serviceToJson(ServiceValue serviceValue) throws IOException {
        return writer.writeValueAsString(serviceValue);
    }

    private String readJson(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream))
                .lines()
                .collect(Collectors.joining("\n"));
    }
}
