package com.talentreef.play;

import com.talentreef.kong.ImmutableAuthnValue;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

// TODO: extend so that we can handle multiple route sources for one service, e.g. routes.conf and apiRoutes.conf etc
public class RouteHandler {

    // TODO: take a list of streams and combine them
    public Map<String, List<String>> fromStream(List<InputStream> inputStreams) throws IOException {
        Map<String, List<String>> routesWithMethods = new HashMap<>();

        for (InputStream is : inputStreams) {

            LineIterator lineIterator = IOUtils.lineIterator(is, "UTF-8");
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                if (!line.trim().isEmpty() &&
                    !line.startsWith("#")  &&
                    !line.startsWith("->")) {

                    String[] routeParts = line.split("\\s+");
                    String routeMethod = routeParts[0];
                    String routePath = routeParts[1]
                            .replaceAll(":\\w+", "[^/]*"); // TODO: Determine if this is a good enough regex for most cases

                    List routeMethods = routesWithMethods
                            .getOrDefault(routePath, new ArrayList<>());
                    routeMethods.add(routeMethod);
                    routesWithMethods.put(routePath, routeMethods);
                }
            }
        }

        return routesWithMethods;
    }

    // TODO: determine how to retrieve hostname so that it works for the migration as well as looking forward
    public List<com.talentreef.kong.RouteValue> toKongRoutes(
            String host,
            Map<String, List<String>> playRoutes) {

        List<com.talentreef.kong.RouteValue> kongRoutes = new ArrayList<>();
        Iterator it = playRoutes.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            kongRoutes.add(
                    new com.talentreef.kong.ImmutableRouteValue.Builder()
                            .hosts(List.of(host))
                            .paths(List.of((String)pair.getKey()))
                            .methods((List<String>)pair.getValue())
                            .authn(
                                    new ImmutableAuthnValue.Builder()
                                            .authenticated(false)
                                            .build()) // TODO: this needs to be enabled with the correct group(s)
                            .build());
        }

        return kongRoutes;
    }
}
