package com.talentreef.play;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RouteFileUtil {

    public static List<InputStream> routeToStream(List<String> routeFiles) throws IOException {
        List<InputStream> inputStreams = new ArrayList<>();
        for (String routeFile: routeFiles) {
            inputStreams.add(FileUtils.openInputStream(new File(routeFile)));
        }
        return inputStreams;
    }
}
