package com.talentreef;


import com.talentreef.command.GenerateServiceDefinition;
import com.talentreef.command.MigrateApiToService;
import com.talentreef.json.ServiceJsonHandler;
import com.talentreef.kong.ServiceDefinitionBuilder;
import com.talentreef.play.RouteHandler;
import picocli.CommandLine;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.Callable;

// TODO: It would be really nice to find a sane way to define the hosts and authorizedGroups for routes prior to generating everything
// TODO: display the usage stuff if no arguments/subcommands are provided
@CommandLine.Command(name = "Play2Kong")
public class KongServiceDefinitionGenerator implements Callable<Integer> {

    @CommandLine.Option(
            names = { "-h", "--help" },
            usageHelp = true,
            description = "Display this help and exit")
    boolean help;

    public static void main(String[] args) throws IOException  {
        ServiceJsonHandler serviceJsonHandler = new ServiceJsonHandler();
        RouteHandler routeHandler = new RouteHandler();
        ServiceDefinitionBuilder serviceDefinitionBuilder =
                new ServiceDefinitionBuilder(serviceJsonHandler, routeHandler);
        new CommandLine(
                new KongServiceDefinitionGenerator())
                .addSubcommand("generate",
                        new GenerateServiceDefinition(serviceDefinitionBuilder))
                .addSubcommand("migrate",
                        new MigrateApiToService(
                                loadConfig("config.properties")
                                        .getProperty("base-url"),
                                serviceDefinitionBuilder))
                .execute(args);
    }

    private static Properties loadConfig(String fileName) throws IOException {
        Properties properties = new Properties();
        InputStream inputStream =
                KongServiceDefinitionGenerator.class
                        .getClassLoader()
                        .getResourceAsStream(fileName);
        properties.load(inputStream);
        return properties;
    }

    public Integer call() {
        return 0;
    }
}