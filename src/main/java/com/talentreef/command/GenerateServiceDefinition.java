package com.talentreef.command;

import com.talentreef.kong.ServiceDefinitionBuilder;
import com.talentreef.kong.ServiceValue;
import com.talentreef.play.RouteFileUtil;
import org.apache.commons.io.FileUtils;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "generate")
public class GenerateServiceDefinition implements Callable<Integer> {
    @CommandLine.Mixin
    private PlayRoutesOption playRoutes;

    @CommandLine.Option(
            names = {"-s", "--service-name"},
            required = true,
            description = "Name of the service to generate a definition for")
    private String serviceName;

    private final ServiceDefinitionBuilder serviceDefinitionBuilder;

    public GenerateServiceDefinition(
            ServiceDefinitionBuilder serviceDefinitionBuilder) {

        this.serviceDefinitionBuilder = serviceDefinitionBuilder;
    }

    public Integer call() throws IOException {

        ServiceValue serviceValueWithRoute = serviceDefinitionBuilder
                .fromStream(
                        serviceName,
                        RouteFileUtil.routeToStream(playRoutes.playRouteSources));

        System.out.println("Writing service definition file for " + serviceName);
        // TODO: return the appropriate status code depending on the success of everything in this method
        serviceDefinitionBuilder.write(
                new File(serviceName + ".json"),
                serviceValueWithRoute);

        return 0;
    }
}
