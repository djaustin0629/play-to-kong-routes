package com.talentreef.command;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.talentreef.kong.ImmutableServiceValue;
import com.talentreef.kong.KongResponse;
import com.talentreef.kong.ServiceDefinitionBuilder;
import com.talentreef.kong.ServiceValue;
import com.talentreef.play.RouteFileUtil;
import org.apache.commons.io.FileUtils;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "migrate")
public class MigrateApiToService implements Callable<Integer> {

    @CommandLine.Option(
            names = { "-a", "--api-id" },
            description = "IDs of the API in kong to migrate")
    private List<String> apiIds;

    @CommandLine.Mixin
    private PlayRoutesOption playRoutes;

    @CommandLine.Option(
            names = { "-l", "--list" },
            description = "Display API JSON")
    private boolean listApiJson;

    private ServiceDefinitionBuilder serviceDefinitionBuilder;
    private String baseUrl;
    public MigrateApiToService(String baseUrl, ServiceDefinitionBuilder serviceDefinitionBuilder) {
        this.serviceDefinitionBuilder = serviceDefinitionBuilder;
        this.baseUrl = baseUrl;
    }

    public Integer call() throws URISyntaxException, IOException, InterruptedException {
        ObjectMapper objectMapper = new ObjectMapper();

        if (listApiJson) {

            String res = sendRequest(
                    buildGet(baseUrl),
                    HttpResponse.BodyHandlers.ofString());
            System.out.println(res);

        } else {

            // TODO: what do we actually need from the kong api?
            String apiId = apiIds.get(0);
            KongResponse kongApiData = objectMapper.readValue(
                    sendRequest(
                            buildGet(
                                    String.join("/", baseUrl, apiId)),
                            HttpResponse.BodyHandlers.ofInputStream()),
                    KongResponse.class);

            ServiceValue serviceValueWithRoute =
                    ImmutableServiceValue.copyOf(
                            serviceDefinitionBuilder.fromStream(
                                    apiId,
                                    RouteFileUtil.routeToStream(
                                            playRoutes.playRouteSources)))
                            .withUri(kongApiData.upstreamUrl())
                            .withName(apiId);

            System.out.println("Writing service definition file");
            serviceDefinitionBuilder.write(
                    new File(apiId + ".json"),
                    serviceValueWithRoute);
        }

        return 0;
    }

    private HttpRequest buildGet(String url) throws URISyntaxException {
        return HttpRequest.newBuilder(new URI(url))
                .header("Content-Type", "application/json")
                .build();
    }

    // This allows us to easily get either a String or InputStream (or whatever else HttpReponse.BodyHandler has defined)
    private <T> T sendRequest(HttpRequest req,
                               HttpResponse.BodyHandler<T> bodyHandler)
            throws IOException, InterruptedException {

        return HttpClient.newHttpClient().send(req, bodyHandler).body();
    }
}
