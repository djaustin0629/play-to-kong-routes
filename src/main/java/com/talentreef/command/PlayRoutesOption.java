package com.talentreef.command;

import picocli.CommandLine;

import java.util.List;

@CommandLine.Command
public class PlayRoutesOption {
    @CommandLine.Option(
            names = {"-r", "--routes"},
            description = "Path to the Play routes file(s)",
            required = true)
    public List<String> playRouteSources;
}
